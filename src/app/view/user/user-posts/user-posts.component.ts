import { Component, OnInit } from '@angular/core';
import {PostService} from '../../../service/post.service';
import {ImageUploadService} from '../../../service/image-upload.service';
import {CommentService} from '../../../service/comment.service';
import {NotificationService} from '../../../service/notification.service';
import {Post} from '../../../models/Post';

@Component({
  selector: 'app-user-posts',
  templateUrl: './user-posts.component.html',
  styleUrls: ['./user-posts.component.css']
})
export class UserPostsComponent implements OnInit {

  isUserPostsLoaded = false;
  posts: Post[];

  constructor(private postService: PostService,
              private imageService: ImageUploadService,
              private commentService: CommentService,
              private notificationService: NotificationService) { }

  ngOnInit(): void {
    this.postService.getCurrentUserPosts()
      .subscribe(data => {
        console.log(data);
        this.posts = data;
        this.getPostsImages(this.posts);
        this.getPostsComments(this.posts);
        this.isUserPostsLoaded = true;
      });
  }

  getPostsImages(posts: Post[]): void {
    posts.forEach(p => {
      this.imageService.getPostImage(p.id)
        .subscribe(data => {
          p.image = data.imageBytes;
        });
    });
  }

  getPostsComments(posts: Post[]): void {
    posts.forEach(p => {
      this.commentService.getPostComments(p.id)
        .subscribe(data => {
          p.comments = data;
        });
    });
  }

  removePost(post: Post, index: number): void {
    console.log(post);
    const result = confirm('Do you really want to delete this post?');
    if (result) {
      this.postService.delete(post.id)
        .subscribe(data => {
          this.posts.splice(index, 1);
          this.notificationService.showSnackBar('Post deleted');
        });
    }
  }

  formatImage(img: any): any {
    if (img == null) {
      return null;
    }
    return 'data:image/jpeg;base64,' + img;
  }

  deleteComment(commentId: number, postIndex: number, commentIndex: number): void {
    const post = this.posts[postIndex];

    this.commentService.delete(commentId)
      .subscribe(() => {
        this.notificationService.showSnackBar('Comment removed');
        post.comments.splice(commentIndex, 1);
      });
  }
}
